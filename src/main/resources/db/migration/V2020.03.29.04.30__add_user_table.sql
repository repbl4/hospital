create table if not exists usr (
    id bigint not null auto_increment,
    username varchar(255) not null,
    email varchar(255),
    password varchar(255) not null,
    first_name varchar(255),
    last_name varchar(255),
    primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table if not exists role (
    id bigint not null auto_increment,
    role_name varchar(255),
    primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table if not exists user_roles (
    user_id bigint not null,
    role_id bigint not null
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table user_roles
    add constraint user_role_user_fk
    foreign key (user_id) references usr(id);
alter table user_roles
    add constraint user_role_role_fk
    foreign key (role_id) references role(id);

insert into role (id, role_name) values (1, 'ROLE_ADMIN'), (2, 'ROLE_DOCTOR'), (3, 'ROLE_NURSE');

insert into usr (id, username, password)
    values (1, 'Andrew', '$2y$12$SZPxZGJC0szqh8eaWL.89.NkzeQptVJAuaLQVS7zVqw44Ib1VUC2G');

insert into user_roles(user_id, role_id) values (1, 1);
