alter table appointment
drop column time_pattern,
    add count_time_pattern int,
    add periodicity_time_pattern varchar(255),
    add join_times_of_day varchar(255),
    add join_name_of_days varchar(255);