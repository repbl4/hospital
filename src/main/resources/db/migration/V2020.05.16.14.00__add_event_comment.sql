alter table event
    add column comment text;

alter table appointment
    modify column patient_id bigint not null;
alter table event
    modify column patient_id bigint not null,
    modify column appointment_id bigint not null;
