use hospital;

create table if not exists appointment (
    id bigint not null auto_increment,
    dosage varchar(255),
    name varchar(255),
    period varchar(255),
    status varchar(255),
    time_pattern varchar(255),
    type varchar(255),
    patient_id bigint,
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table if not exists event (
    id bigint not null auto_increment,
    event_status varchar(255),
    event_time datetime,
    appointment_id bigint,
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table if not exists patient (
    id bigint not null auto_increment,
    attached_doctor varchar(255),
    diagnostic varchar(255),
    first_name varchar(255),
    health_status varchar(255),
    insurance_number varchar(255),
    last_name varchar(255),
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table appointment
    add constraint FK_appointment_patient
    foreign key (patient_id)
    references patient (id);

alter table event
    add constraint FK_event_appointment
    foreign key (appointment_id)
    references appointment (id);