package ru.med.hospital.property;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Класс свойств базы данных.
 */

@Data
@Component
@PropertySource("classpath:database.properties")
public class DataProperties {
    @Value("${datasource.url}")
    private String url;
    @Value("${datasource.username}")
    private String username;
    @Value("${datasource.password}")
    private String password;
    @Value("${datasource.driver-name}")
    private String driverClassName;
}
