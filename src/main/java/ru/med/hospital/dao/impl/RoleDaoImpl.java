package ru.med.hospital.dao.impl;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.med.hospital.dao.RoleDao;
import ru.med.hospital.model.Role;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Component
@Transactional(readOnly = true)
public class RoleDaoImpl implements RoleDao {
	@PersistenceContext(unitName = "entityManagerFactory")
	private EntityManager em;

	@Override
	public Role getRoleByName(String roleName) {
		TypedQuery<Role> query = em.createQuery("select r from Role r where r.roleName = :roleName", Role.class);
		query.setParameter("roleName", roleName);
		return query.getSingleResult();
	}
}
