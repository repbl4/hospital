package ru.med.hospital.dao.impl;


import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.med.hospital.dao.AppointmentDao;
import ru.med.hospital.model.Appointment;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import java.util.List;

@Component
@Scope(proxyMode = ScopedProxyMode.INTERFACES)
@Transactional(readOnly = true)
public class AppointmentDaoImpl implements AppointmentDao {

    @PersistenceContext(unitName = "entityManagerFactory")
    private EntityManager em;

    @Override
    public Appointment getAppointment(Long id) {
        TypedQuery<Appointment> query =
                em.createQuery("select a from Appointment a " +
                        "where a.id = :id", Appointment.class);
        query.setParameter("id", id);
        return query.getResultList().stream().findFirst().orElseThrow(
                () -> new RuntimeException("Назначение пациенту с id: " + id + " отсутствует")
        );
    }

    @Override
    public List<Appointment> getAppointmentsByPatientId(Long id) {
        TypedQuery<Appointment> query
            = em.createQuery("select a from Appointment a where a.patient.id = :patientId", Appointment.class);
        query.setParameter("patientId", id);
        return query.getResultList();
    }

    @Override
    @Transactional
    public Appointment createAppointment(Appointment appointment) {
        em.persist(appointment);
        return appointment;
    }

    @Override
    @Transactional
    public Appointment updateAppointment(Appointment appointment) {
        em.merge(appointment);
        em.flush();
        return appointment;
    }
}
