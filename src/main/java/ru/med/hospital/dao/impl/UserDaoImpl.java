package ru.med.hospital.dao.impl;


import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.med.hospital.dao.UserDao;
import ru.med.hospital.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import java.util.List;

@Component
@Transactional(readOnly = true)
public class UserDaoImpl implements UserDao {
	@PersistenceContext(unitName = "entityManagerFactory")
	private EntityManager em;

	@Override
	public User findByUsername(String username) {
		TypedQuery<User> query =
			em.createQuery("select u from User u where u.username = :username",
				User.class);
		query.setParameter("username", username);
		return query.getResultList().stream().findAny().orElse(null);
	}

	@Override
	public List<User> findAllUsers() {
		return em.createQuery("select u from User u", User.class)
			.getResultList();
	}

	@Override
	@Transactional
	public User createUser(User user) {
		em.persist(user);
		return user;
	}
}
