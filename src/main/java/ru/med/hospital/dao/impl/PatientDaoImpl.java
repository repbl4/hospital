package ru.med.hospital.dao.impl;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.med.hospital.dao.PatientDao;
import ru.med.hospital.exception.PatientWithIdNotFoundException;
import ru.med.hospital.model.Patient;
import ru.med.hospital.model.enums.HealthStatus;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import java.util.List;

@Component
@Transactional(readOnly = true)
public class PatientDaoImpl implements PatientDao {
    @PersistenceContext(unitName = "entityManagerFactory")
    private EntityManager em;

    @Override
    public List<Patient> getAllPatients() {
        return em.createQuery(
                "select p from Patient p", Patient.class)
                .getResultList();
    }

    @Override
    public Patient getPatientById(Long id) throws PatientWithIdNotFoundException {
        TypedQuery<Patient> query = em.createQuery(
                "select p from Patient p where p.id = :id",
                Patient.class
        );
        query.setParameter("id", id);
        return query.getResultList().stream().findFirst().orElseThrow(
            () -> new PatientWithIdNotFoundException(id)
        );
    }

    @Override
    public List<Patient> getAllPatientsByStatus(String status) {
        HealthStatus healthStatus = HealthStatus.valueOf(status);
        TypedQuery<Patient> query =
            em.createQuery("select p from Patient p where p.healthStatus = :healthStatus", Patient.class);
        query.setParameter("healthStatus", healthStatus);
        return query.getResultList();
    }

    @Override
    @Transactional
    public Patient addPatient(Patient patient) {
        em.persist(patient);
        return patient;
    }

    @Override
    @Transactional
    public Patient updatePatient(Patient patient) {
        em.merge(patient);
        em.flush();
        return patient;
    }
}
