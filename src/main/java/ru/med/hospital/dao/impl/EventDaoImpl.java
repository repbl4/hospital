package ru.med.hospital.dao.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.med.hospital.dao.EventDao;
import ru.med.hospital.model.Event;
import ru.med.hospital.model.enums.EventStatus;
import ru.med.hospital.util.DateUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Component
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class EventDaoImpl implements EventDao {

    @PersistenceContext(unitName = "entityManagerFactory")
    private EntityManager em;

    @Override
    public List<Event> getAllEvents() {
        return em.createQuery("select e from Event e", Event.class).getResultList();
    }

    @Override
    public Event getEvent(Long id) {
        TypedQuery<Event> query =
                em.createQuery("select e from Event e where e.id =:id", Event.class);
        query.setParameter("id", id);
        return query.getResultList().stream().findFirst().orElseThrow(
                () -> new RuntimeException("Event with id: " + id + " not found")
        );
    }

    @Override
    @Transactional
    public void createEvent(Event event) {
        em.persist(event);
    }

    @Override
    @Transactional
    public Event updateEvent(Event event) {
        em.merge(event);
        em.flush();
        return event;
    }

    @Override
    public List<Event> getAllEventsByPatient(Long id) {
        TypedQuery<Event> query =
                em.createQuery("select e from Event e " +
                        "where e.patient.id = :id", Event.class);
        query.setParameter("id", id);
        return query.getResultList();
    }

    @Override
    public List<Event> getPlannedEventsByAppointmentId(Long id) {
        EventStatus eventStatus = EventStatus.PLANNED;
        TypedQuery<Event> query =
            em.createQuery("select e from Event e " +
                "where e.appointment.id = :id and e.eventStatus = :eventStatus", Event.class);
        query.setParameter("id", id);
        query.setParameter("eventStatus", eventStatus);
        return query.getResultList();
    }

    @Override
    public List<Event> getAllEventsToday() {
        Date endOfDay = DateUtils.getEndOfDay();
        EventStatus status = EventStatus.PLANNED;

        TypedQuery<Event> query = em.createQuery("select e from Event e where " +
                "e.eventTime between current_time and :endOfDay and e.eventStatus = :status",
            Event.class);
        query.setParameter("endOfDay", endOfDay);
        query.setParameter("status", status);
        return query.getResultList();
    }

    @Override
    public List<Event> getAllEventsDuringNextThreeHours() {
        Date inThreeHoursAfterNow = DateUtils.getThreeNextHours();
        TypedQuery<Event> query = em.createQuery("select e from Event e where " +
            "e.eventTime between current_time and :inThreeHoursAfterNow", Event.class);
        query.setParameter("inThreeHoursAfterNow", inThreeHoursAfterNow);
        return query.getResultList();
    }
    @Override
    @Transactional
    public void deleteEvent(Event event) {
        em.remove(event);
    }
}
