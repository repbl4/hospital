package ru.med.hospital.dao;

import ru.med.hospital.model.Role;

public interface RoleDao {
	Role getRoleByName(String roleName);
}
