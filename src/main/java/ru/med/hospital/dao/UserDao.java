package ru.med.hospital.dao;

import ru.med.hospital.model.User;

import java.util.List;

public interface UserDao {
	User findByUsername(String username);

	User createUser(User user);

	List<User> findAllUsers();

}
