package ru.med.hospital.dao;

import ru.med.hospital.exception.PatientWithIdNotFoundException;
import ru.med.hospital.model.Patient;

import java.util.List;

public interface PatientDao {
    List<Patient> getAllPatients();

    List<Patient> getAllPatientsByStatus(String healthStatus);

    Patient getPatientById(Long id) throws PatientWithIdNotFoundException;

    Patient addPatient(Patient patient);

    Patient updatePatient(Patient patient);
}
