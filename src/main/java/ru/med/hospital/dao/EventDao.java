package ru.med.hospital.dao;

import ru.med.hospital.model.Event;

import java.util.List;

public interface EventDao {
    List<Event> getAllEvents();
    List<Event> getAllEventsToday();
    List<Event> getAllEventsDuringNextThreeHours();
    List<Event> getAllEventsByPatient(Long id);
    List<Event> getPlannedEventsByAppointmentId(Long id);
    Event getEvent(Long id);
    void createEvent(Event event);
    void deleteEvent(Event event);
    Event updateEvent(Event event);
}
