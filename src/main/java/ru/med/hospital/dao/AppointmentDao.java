package ru.med.hospital.dao;

import ru.med.hospital.model.Appointment;

import java.util.List;

public interface AppointmentDao {
    List<Appointment> getAppointmentsByPatientId(Long id);
    Appointment getAppointment(Long id);
    Appointment createAppointment(Appointment appointment);
    Appointment updateAppointment(Appointment appointment);
}
