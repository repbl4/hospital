package ru.med.hospital.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import ru.med.hospital.exception.JwtAuthenticationException;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Component
@RequiredArgsConstructor
@PropertySource("classpath:application.properties")
@Slf4j
public class JwtTokenProvider {
	private final UserDetailsService userDetailsService;

	@Value("${jwt.token.secret}")
	private String secret;
	@Value("${jwt.token.expired}")
	private int expired;

	@PostConstruct
	public void init() {
		secret = Base64.getEncoder().encodeToString(secret.getBytes());
	}

	public String createToken(UserDetails userDetails) {
		String username = userDetails.getUsername();
		Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
		Claims claims = Jwts.claims().setSubject(username);
		claims.put("roles", getRoleNames(authorities));
		DateTimeZone dateTimeZone = DateTimeZone.forID("Europe/Moscow");
		LocalDateTime now = LocalDateTime.now(dateTimeZone);
		LocalDateTime validity = now.plusSeconds(expired);

		return "Bearer " + Jwts.builder()
			.setClaims(claims)
			.setIssuedAt(now.toDate())
			.setExpiration(validity.toDate())
			.signWith(SignatureAlgorithm.HS256, secret)
			.compact();
	}

	public Authentication getAuthentication(String token) {
		UserDetails userDetails = this.userDetailsService.loadUserByUsername(getUsername(token));
		return new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
	}

	public String getUsername(String token) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
	}

	public String resolveToken(HttpServletRequest req) {
		String bearerToken = req.getHeader("Authorization");
		if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}

	public boolean validateToken(String token) {
		try {
			Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
			return !claims.getBody().getExpiration().before(new Date());
		} catch (SignatureException e) {
			log.error("Invalid JWT signature -> Message: {} ", e.getMessage());
			throw new JwtAuthenticationException();
		} catch (MalformedJwtException e) {
			log.error("Invalid JWT token -> Message: {}", e.getMessage());
			throw new JwtAuthenticationException();
		} catch (ExpiredJwtException e) {
			log.error("Expired JWT token -> Message: {}", e.getMessage());
			throw new JwtAuthenticationException();
		} catch (UnsupportedJwtException e) {
			log.error("Unsupported JWT token -> Message: {}", e.getMessage());
			throw new JwtAuthenticationException();
		} catch (IllegalArgumentException e) {
			log.error("JWT claims string is empty -> Message: {}", e.getMessage());
			throw new JwtAuthenticationException();
		}
	}

	public List<String> getRoleNames(Collection<? extends GrantedAuthority> roles) {
		List<String> roleNames = new ArrayList<>();
		roles.forEach(role -> {
			roleNames.add(role.getAuthority());
		});
		return roleNames;
	}
}
