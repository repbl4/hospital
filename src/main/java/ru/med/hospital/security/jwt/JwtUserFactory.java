package ru.med.hospital.security.jwt;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import ru.med.hospital.model.Role;
import ru.med.hospital.model.User;

import java.util.List;
import java.util.stream.Collectors;

public final class JwtUserFactory {
	public JwtUserFactory() {}

	public static JwtUser createUser(User user) {
		return new JwtUser(
			user.getId(),
			user.getUsername(),
			user.getFirstName(),
			user.getLastName(),
			user.getEmail(),
			user.getPassword(),
			mapToGrantedAuthority(user.getRoles())
		);
	}

	public static List<GrantedAuthority> mapToGrantedAuthority(List<Role> roles) {
		return roles.stream()
			.map(role ->
				new SimpleGrantedAuthority(role.getRoleName())
			).collect(Collectors.toList());
	}
}
