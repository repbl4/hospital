package ru.med.hospital.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.med.hospital.dao.UserDao;
import ru.med.hospital.model.User;
import ru.med.hospital.security.jwt.JwtUserFactory;

@Service
@RequiredArgsConstructor
public class JwtUserServiceDetail implements UserDetailsService {
	private final UserDao userDao;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = this.userDao.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("User with username: " + username + " not found");
		}
		return JwtUserFactory.createUser(user);
	}
}
