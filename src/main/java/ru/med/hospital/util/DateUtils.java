package ru.med.hospital.util;

import org.joda.time.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import ru.med.hospital.model.enums.NameOfWeekDays;
import ru.med.hospital.model.enums.TimeOfDay;

import java.util.*;


public class DateUtils {
    private static Map<NameOfWeekDays, Integer> mapWeekDays = new HashMap<>();
    private static Map<TimeOfDay, String> mapTimeOfDay = new HashMap<>();
    private static final DateTimeZone DATE_TIME_ZONE;

    static {
        DATE_TIME_ZONE = DateTimeZone.forID("Europe/Moscow");
        mapWeekDays.put(NameOfWeekDays.MONDAY, 1);
        mapWeekDays.put(NameOfWeekDays.TUESDAY, 2);
        mapWeekDays.put(NameOfWeekDays.WEDNESDAY, 3);
        mapWeekDays.put(NameOfWeekDays.THURSDAY, 4);
        mapWeekDays.put(NameOfWeekDays.FRIDAY, 5);
        mapWeekDays.put(NameOfWeekDays.SATURDAY, 6);
        mapWeekDays.put(NameOfWeekDays.SUNDAY, 7);

        mapTimeOfDay.put(TimeOfDay.MORNING, "8:30");
        mapTimeOfDay.put(TimeOfDay.NOON, "14:00");
        mapTimeOfDay.put(TimeOfDay.EVENING, "20:00");
    }

    public static List<Date> getDatesForProcedure(Integer period, String day, int time) {
        LocalDateTime now = LocalDateTime.now(DATE_TIME_ZONE);
        List<Date> dateList = new ArrayList<>();
        for (int i = 1; i <= period; i++) {
            LocalDateTime days = now.plusDays(i).withTime(time, 0, 0, 0);
            if (days.dayOfWeek().get() == mapWeekDays.get(NameOfWeekDays.valueOf(day))) {
                dateList.add(days.toDate());
            }
        }
        return dateList;
    }

    public static List<Date> getDatesForMedicament(Integer period, List<String> timesOfDay) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("HH:mm");
        LocalDateTime now = LocalDateTime.now(DATE_TIME_ZONE);
        List<Date> dateList = new ArrayList<>();
        for (int i = 0; i < period; i++) {
            for (String timeOfDay: timesOfDay) {
                String time = getTime(timeOfDay);
                LocalDateTime days = now.plusDays(i).withTime(
                        LocalDateTime.parse(time, dateTimeFormatter).getHourOfDay(),
                        LocalDateTime.parse(time, dateTimeFormatter).getMinuteOfHour(), 0, 0
                );
                dateList.add(days.toDate());
            }
        }
        return dateList;
    }

    private static String getTime(String time) {
        TimeOfDay timeOfDay = TimeOfDay.valueOf(time);
        switch (timeOfDay) {
            case MORNING:
                return mapTimeOfDay.get(TimeOfDay.MORNING);
            case NOON:
                return mapTimeOfDay.get(TimeOfDay.NOON);
            case EVENING:
                return mapTimeOfDay.get(TimeOfDay.EVENING);
            default:
                throw new RuntimeException("No such time of day");
        }
    }

    public static Date getEndOfDay() {
        LocalDateTime now = LocalDateTime.now(DATE_TIME_ZONE);
        return now.withTime(23,59,0,0).toDate();
    }

    public static Date getThreeNextHours() {
        LocalDateTime now = LocalDateTime.now(DATE_TIME_ZONE);
        return now.plusHours(3).toDate();
    }

}
