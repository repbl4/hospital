package ru.med.hospital.util;

public interface Convertible<T> {
    T convertToDTO();
}
