package ru.med.hospital.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class PropertyLoader {

    public Property property = new Property();

    public void loadProperties() {
        File file = new File("config.properties");
        Map<String, String> properties = new HashMap<>();
        try {
            BufferedReader r = new BufferedReader(new FileReader(file));
            String line;
            while ((line = r.readLine()) != null) {
                String n = line.substring(0, line.indexOf(":"));
                String v = line.substring(line.indexOf(":") + 1, line.length());
                properties.put(n, v);
            }
            property.setProperties(properties);
        } catch (Exception e) {}
    }

    public String get(String key) {
        return property.getProperties().get(key);
    }

    private static class Property {
        private Map<String, String> properties = new HashMap<>();

        public Property() {
        }

        public Map<String, String> getProperties() {
            return properties;
        }

        public void setProperties(Map<String, String> properties) {
            this.properties = properties;
        }
    }
}
