package ru.med.hospital.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.med.hospital.dto.UserDto;
import ru.med.hospital.util.Convertible;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import java.util.List;

@Data
@Entity
@Table(name = "usr")
public class User implements Convertible<UserDto> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "username")
	private String username;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "email")
	private String email;
	@Column(name = "password")
	private String password;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_roles",
			joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
			inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	private List<Role> roles;
	@Override
	public UserDto convertToDTO() {
		return new UserDto(
			this.getUsername(),
			this.getFirstName(),
			this.getLastName(),
			this.getEmail()
		);
	}
}
