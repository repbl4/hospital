package ru.med.hospital.model;

import lombok.Data;
import ru.med.hospital.dto.EventDto;
import ru.med.hospital.model.enums.EventStatus;
import ru.med.hospital.util.Convertible;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.util.Date;

/**
 * Сущность - событие.
 */

@Data
@Entity
@Table(name = "event")
public class Event implements Convertible<EventDto> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "event_time")
	private Date eventTime;
	@Enumerated(EnumType.STRING)
	@Column(name = "event_status")
	private EventStatus eventStatus;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "appointment_id")
	private Appointment appointment;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "patient_id")
	private Patient patient;
	@Column(name = "comment", columnDefinition = "text")
	private String comment;

	@Override
	public EventDto convertToDTO() {
		return new EventDto(
			this.getId(),
			this.getEventTime(),
			this.getEventStatus() == null ? null : this.getEventStatus().getEventStatus(),
			this.getAppointment() == null ? null : this.getAppointment().getName(),
			this.getAppointment() == null ? null : this.getAppointment().getType().getType(),
			this.getPatient() == null ? null : this.getPatient().getFirstName(),
			this.getPatient() == null ? null : this.getPatient().getLastName(),
			this.getComment()
		);
	}
}
