package ru.med.hospital.model.enums;

/**
 * Времена суток для удобства работы с событиями.
 */

public enum TimeOfDay {
    MORNING("утро"),
    NOON("день"),
    EVENING("вечер");

    private String dayTime;

    TimeOfDay(String dayTime) {
        this.dayTime = dayTime;
    }

    public String getDayTime() {
        return dayTime;
    }
}
