package ru.med.hospital.model.enums;

/**
 * Статус назначения.
 */

public enum AppointmentStatus {
    ASSIGNED("ASSIGNED"),
    CANCELED("CANCELED");

    private String status;

    AppointmentStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
