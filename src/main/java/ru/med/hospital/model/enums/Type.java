package ru.med.hospital.model.enums;

/**
 * Тип назначения.
 */

public enum Type {
    MEDICAMENT("MEDICAMENT"),
    PROCEDURE("PROCEDURE");

    private final String type;

    Type(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
