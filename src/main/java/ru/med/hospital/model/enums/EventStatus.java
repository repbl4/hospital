package ru.med.hospital.model.enums;

/**
 * Статус события.
 */

public enum EventStatus {
    PLANNED("PLANNED"),
    PERFORMED("PERFORMED"),
    CANCELED("CANCELED");

    private final String eventStatus;

    EventStatus(String eventStatus) {
        this.eventStatus = eventStatus;
    }

    public String getEventStatus() {
        return eventStatus;
    }
}
