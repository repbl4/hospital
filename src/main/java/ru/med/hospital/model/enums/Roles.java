package ru.med.hospital.model.enums;

import lombok.Getter;

public enum Roles {
	ADMIN ("ADMIN"),
	DOCTOR ("DOCTOR"),
	NURSE ("NURSE");

	@Getter
	private String roleName;

	Roles(String roleName) {
		this.roleName = roleName;
	}
}
