package ru.med.hospital.model.enums;

/**
 * Статус пациента.
 */

public enum HealthStatus {
    HEALTHY("HEALTHY"),
    UNHEALTHY("UNHEALTHY");

    private final String healthStatus;

    HealthStatus(String status) {
        this.healthStatus = status;
    }

    public String getHealthStatus() {
        return healthStatus;
    }
}
