package ru.med.hospital.model;

import lombok.Data;
import ru.med.hospital.dto.AppointmentDto;
import ru.med.hospital.dto.TimePatternDto;
import ru.med.hospital.model.enums.AppointmentStatus;
import ru.med.hospital.model.enums.Type;
import ru.med.hospital.util.Convertible;

import javax.persistence.*;
import java.util.Arrays;

/**
 * Сущность - назначение.
 */

@Data
@Entity
@Table(name = "appointment")
public class Appointment implements Convertible<AppointmentDto> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private Type type;
    @Column(name = "name")
    private String name;
    @Column(name = "count_time_pattern")
    private Integer countTimePattern;
    @Column(name = "periodicity_time_pattern")
    private String periodicityTimePattern;
    @Column(name = "join_times_of_day")
    private String joinTimesOfDay;
    @Column(name = "join_name_of_days")
    private String joinNameOfDays;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id")
    private Patient patient;
    @Column(name = "period")
    private String period;
    @Column(name = "dosage")
    private String dosage;
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private AppointmentStatus status;

    @Override
    public AppointmentDto convertToDTO() {
        return new AppointmentDto(
                this.getId(),
                this.patient == null ? null : this.patient.getId(),
                this.getType() == null ? null : this.getType().getType(),
                this.getName(),
                new TimePatternDto(
                        this.getJoinTimesOfDay() == null ? null: Arrays.asList(this.getJoinTimesOfDay().split(", ")),
                        this.getJoinNameOfDays() == null ? null : Arrays.asList(this.getJoinNameOfDays().split(", "))
                ),
                this.getPeriod(),
                this.getDosage(),
                this.getStatus() == null ? null : this.getStatus().getStatus()
        );
    }
}
