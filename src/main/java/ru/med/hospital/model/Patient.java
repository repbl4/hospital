package ru.med.hospital.model;

import lombok.Data;
import ru.med.hospital.dto.PatientDto;
import ru.med.hospital.model.enums.HealthStatus;
import ru.med.hospital.util.Convertible;

import javax.persistence.*;

/**
 * Сущность - пациент.
 */

@Data
@Entity
@Table(name = "patient")
public class Patient implements Convertible<PatientDto> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "diagnostic")
    private String diagnostic;
    @Column(name = "insurance_number")
    private String insuranceNumber;
    @Column(name = "attached_doctor")
    private String attachedDoctor;
    @Enumerated(EnumType.STRING)
    @Column(name = "health_status")
    private HealthStatus healthStatus;

    @Override
    public PatientDto convertToDTO() {
        return new PatientDto(
                this.getId(),
                this.getFirstName(),
                this.getLastName(),
                this.getDiagnostic(),
                this.getInsuranceNumber(),
                this.getAttachedDoctor(),
                this.getHealthStatus().getHealthStatus()
        );
    }
}
