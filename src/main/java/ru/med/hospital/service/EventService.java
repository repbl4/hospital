package ru.med.hospital.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.med.hospital.dao.EventDao;
import ru.med.hospital.dto.EventDto;
import ru.med.hospital.exception.EventStatusNotFoundException;
import ru.med.hospital.model.Appointment;
import ru.med.hospital.model.Event;
import ru.med.hospital.model.enums.EventStatus;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EventService {

    private final EventDao eventDao;

    public Event getEventEntity(Long id) {
        return this.eventDao.getEvent(id);
    }

    public List<EventDto> getAllEventsByPatient(Long id) {
        return this.eventDao.getAllEventsByPatient(id)
                .stream()
                .map(Event::convertToDTO)
                .collect(Collectors.toList());
    }

    public List<Event> getPlannedEventsByAppointmentId(Long id) {
        return this.eventDao.getPlannedEventsByAppointmentId(id);
    }

    public void deleteEvent(Event event) {
        this.eventDao.deleteEvent(event);
    }

    public EventDto getEventDto(Long id) {
        return this.eventDao.getEvent(id).convertToDTO();
    }

    public List<EventDto> getAllEvents() {
        return this.eventDao.getAllEvents()
                .stream()
                .map(Event::convertToDTO)
                .collect(Collectors.toList());
    }

    public void createEvent(Appointment appointment, Date date) {
        Event event = new Event();
        event.setAppointment(appointment);
        event.setPatient(appointment.getPatient());
        event.setEventStatus(EventStatus.PLANNED);
        event.setEventTime(date);
        this.eventDao.createEvent(event);
    }

    public EventDto updateEvent(Long id, EventDto dto) throws EventStatusNotFoundException {
        Event event = this.getEventEntity(id);
        event.setComment(dto.getComment());
        event.setEventStatus(this.convertStatus(dto.getStatus()));
         return this.eventDao.updateEvent(event).convertToDTO();
    }

    public List<EventDto> getEventForToday() {
        return this.eventDao.getAllEventsToday()
                .stream()
                .map(Event::convertToDTO)
                .collect(Collectors.toList());
    }

    public List<EventDto> getEventsDuringNextThreeHours() {
        return this.eventDao.getAllEventsDuringNextThreeHours()
            .stream()
            .map(Event::convertToDTO)
            .collect(Collectors.toList());
    }

    private EventStatus convertStatus(String status) throws EventStatusNotFoundException {
        switch (status) {
            case "PLANNED":
                return EventStatus.PLANNED;
            case "PERFORMED":
                return EventStatus.PERFORMED;
            case "CANCELED":
                return EventStatus.CANCELED;
            default:
                throw new EventStatusNotFoundException(status);
        }
    }
}
