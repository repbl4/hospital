package ru.med.hospital.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.med.hospital.dao.AppointmentDao;
import ru.med.hospital.dto.AppointmentDto;
import ru.med.hospital.dto.TimePatternDto;
import ru.med.hospital.dto.UpdatedAppointDto;
import ru.med.hospital.exception.AppointmentNotFound;
import ru.med.hospital.exception.PatientWithIdNotFoundException;
import ru.med.hospital.exception.TimePatternNotFoundException;
import ru.med.hospital.model.*;
import ru.med.hospital.model.enums.AppointmentStatus;
import ru.med.hospital.model.enums.Type;
import ru.med.hospital.util.DateUtils;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AppointmentService {
    private final AppointmentDao appointmentDao;
    private final PatientService patientService;
    private final EventService eventService;


    public AppointmentDto getAppointmentDto(Long id) {
        return this.appointmentDao.getAppointment(id).convertToDTO();
    }
    public Appointment getAppointment(Long id) {
        return this.appointmentDao.getAppointment(id);
    }

    public List<AppointmentDto> getAppointmentsByPatientId(Long id) {
        return this.appointmentDao.getAppointmentsByPatientId(id)
            .stream()
            .map(Appointment::convertToDTO)
            .collect(Collectors.toList());
    }

    public AppointmentDto createAppointment(AppointmentDto dto) throws PatientWithIdNotFoundException {
        Appointment appointment = this.convertDtoToEntity(dto);
        appointment.setStatus(AppointmentStatus.ASSIGNED);

        AppointmentDto appointmentDto = this.appointmentDao.createAppointment(appointment).convertToDTO();
        this.createEventProcedure(dto, appointment);
        this.createEventMedicament(dto, appointment);
        return appointmentDto;
    }

    @Transactional
    public AppointmentDto updateAppointment(Long id, UpdatedAppointDto dto)
        throws TimePatternNotFoundException, AppointmentNotFound {
        Appointment appointment = this.getAppointment(id);
        if (appointment == null || appointment.getStatus().equals(AppointmentStatus.CANCELED)) {
            throw new AppointmentNotFound();
        }
        List<Event> allEventsByAppointmentId = this.eventService.getPlannedEventsByAppointmentId(appointment.getId());
        Iterator<Event> eventIterator = allEventsByAppointmentId.iterator();
        while (eventIterator.hasNext()) {
            this.eventService.deleteEvent(eventIterator.next());
        }
        if (dto.getName() != null && !dto.getName().isEmpty()) {
            appointment.setName(dto.getName());
        }
        if (dto.getStatus() != null && !dto.getStatus().isEmpty()) {
            appointment.setStatus(AppointmentStatus.CANCELED);
        }
        if (dto.getDosage() != null && !dto.getDosage().isEmpty()) {
            appointment.setDosage(dto.getDosage());
        }
        if (dto.getPeriod() != null && !dto.getPeriod().isEmpty()) {
            appointment.setPeriod(dto.getPeriod());
        }
        TimePatternDto timePatternDto = dto.getTimePatternDto();
        if (timePatternDto == null) {
            throw new TimePatternNotFoundException();
        }
        if (timePatternDto.getTimeOfDay() != null && !timePatternDto.getTimeOfDay().isEmpty()) {
            appointment.setJoinTimesOfDay(String.join(", ", timePatternDto.getTimeOfDay()));
        }
        if (timePatternDto.getNameOfDays() != null && !timePatternDto.getNameOfDays().isEmpty()) {
            appointment.setJoinNameOfDays(String.join(", ", timePatternDto.getNameOfDays()));
        }
        Appointment currentAppointment = this.appointmentDao.updateAppointment(appointment);
        AppointmentDto appointmentDto = currentAppointment.convertToDTO();
        if (!currentAppointment.getStatus().equals(AppointmentStatus.CANCELED)) {
            this.createEventProcedure(appointmentDto, appointment);
            this.createEventMedicament(appointmentDto, appointment);
        }
        return appointmentDto;
    }

    private Appointment convertDtoToEntity(AppointmentDto dto) throws PatientWithIdNotFoundException {
        Appointment appointment = new Appointment();
        Type type = this.defineType(dto.getType());
        String name = dto.getName();
        TimePatternDto timePatternDto = dto.getTimePatternDto();
        appointment.setPatient(this.patientService.getPatientEntity(dto.getPatientId()));
        appointment.setDosage(dto.getDosage());
        appointment.setPeriod(dto.getPeriod());
        appointment.setName(name);
        appointment.setType(type);
        if (timePatternDto.getTimeOfDay() != null) {
            appointment.setJoinTimesOfDay(String.join(", ", timePatternDto.getTimeOfDay()));
        }
        if (timePatternDto.getNameOfDays() != null) {
            appointment.setJoinNameOfDays(String.join(", ", timePatternDto.getNameOfDays()));
        }

        return appointment;
    }

    private Type defineType(String type) {
        if (type.equals("MEDICAMENT")) {
            return Type.MEDICAMENT;
        }
        return Type.PROCEDURE;
    }

    private void createEventProcedure(AppointmentDto dto, Appointment appointment) {
        List<String> nameOfDays = dto.getTimePatternDto().getNameOfDays();
        if (nameOfDays != null && !nameOfDays.isEmpty()) {
            for (String day: nameOfDays) {
                List<Date> dateList = DateUtils.getDatesForProcedure(Integer.parseInt(dto.getPeriod()),
                        day, 14);
                for (Date date: dateList) {
                    this.eventService.createEvent(appointment, date);
                }
            }
        }
    }

    private void createEventMedicament(AppointmentDto dto, Appointment appointment) {
        List<String> timeOfDay = dto.getTimePatternDto().getTimeOfDay();
        if (timeOfDay != null && !timeOfDay.isEmpty()) {
            List<Date> datesForMedicament = DateUtils.getDatesForMedicament(Integer.parseInt(dto.getPeriod()), timeOfDay);
            for (Date date: datesForMedicament) {
                this.eventService.createEvent(appointment, date);
            }
        }
    }
}
