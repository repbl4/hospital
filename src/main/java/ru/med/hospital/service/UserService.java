package ru.med.hospital.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.med.hospital.dao.RoleDao;
import ru.med.hospital.dao.UserDao;
import ru.med.hospital.dto.AuthResponse;
import ru.med.hospital.dto.RegisterUserDto;
import ru.med.hospital.dto.UserDto;
import ru.med.hospital.dto.UserLoginDto;
import ru.med.hospital.exception.UserAlreadyExistException;
import ru.med.hospital.model.Role;
import ru.med.hospital.model.User;
import ru.med.hospital.security.jwt.JwtTokenProvider;
import ru.med.hospital.security.jwt.JwtUser;
import ru.med.hospital.security.jwt.JwtUserFactory;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
	private final JwtTokenProvider jwtTokenProvider;
	private final AuthenticationManager authenticationManager;
	private final UserDao userDao;
	private final RoleDao roleDao;
	private final PasswordEncoder passwordEncoder;

	public User getUserByUsername(String username) {
		return this.userDao.findByUsername(username);
	}

	public List<UserDto> getAllUsers() {
		return this.userDao.findAllUsers()
			.stream()
			.map(User::convertToDTO)
			.collect(Collectors.toList());
	}

	public UserDto registerUser(RegisterUserDto registerUserDto) throws UserAlreadyExistException {
		User userFromDb = this.getUserByUsername(registerUserDto.getUsername());
		if (userFromDb != null) {
			throw new UserAlreadyExistException();
		}
		Role role = this.roleDao.getRoleByName(registerUserDto.getRole());
		User user = new User();
		user.setFirstName(registerUserDto.getFirstName());
		user.setLastName(registerUserDto.getLastName());
		user.setEmail(registerUserDto.getEmail());
		user.setUsername(registerUserDto.getUsername());
		user.setRoles(Collections.singletonList(role));
		user.setPassword(this.passwordEncoder.encode(registerUserDto.getPassword()));
		return this.userDao.createUser(user).convertToDTO();
	}

	public AuthResponse login(UserLoginDto userLoginDto) {
		try {
			String username = userLoginDto.getUsername();
			String password = userLoginDto.getPassword();
			this.authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(username, password));
			User userFromDB = this.getUserByUsername(username);
			if (userFromDB == null) {
				throw new UsernameNotFoundException("User with username: " + username + " not found");
			}
			JwtUser userDetails = JwtUserFactory.createUser(userFromDB);
			String token = this.jwtTokenProvider.createToken(userDetails);
			return new AuthResponse(username, token);
		} catch (AuthenticationException e) {
			throw new BadCredentialsException("Invalid username or password");
		}
	}

}
