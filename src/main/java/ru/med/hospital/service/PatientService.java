package ru.med.hospital.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import ru.med.hospital.dao.EventDao;
import ru.med.hospital.dao.PatientDao;
import ru.med.hospital.dto.PatientDto;
import ru.med.hospital.exception.PatientWithIdCanNotBeDischarge;
import ru.med.hospital.exception.PatientWithIdNotFoundException;
import ru.med.hospital.model.Event;
import ru.med.hospital.model.enums.EventStatus;
import ru.med.hospital.model.enums.HealthStatus;
import ru.med.hospital.model.Patient;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PatientService {
    private final PatientDao patientDao;
    private final EventDao eventDao;

    public List<PatientDto> getPatientsList() {
        return this.patientDao.getAllPatients()
                .stream()
                .map(Patient::convertToDTO)
                .collect(Collectors.toList());
    }

    public PatientDto getPatientDtoById(Long id) throws PatientWithIdNotFoundException {
        return this.patientDao.getPatientById(id).convertToDTO();
    }

    public List<PatientDto> getAllPatientsByStatus(String patientStatus) {
        return this.patientDao.getAllPatientsByStatus(patientStatus)
            .stream()
            .map(Patient::convertToDTO)
            .collect(Collectors.toList());
    }

    public Patient getPatientEntity(Long id) throws PatientWithIdNotFoundException {
        return this.patientDao.getPatientById(id);
    }

    public PatientDto addPatient(PatientDto dto) {
        Patient patient = new Patient();
        BeanUtils.copyProperties(dto, patient, "id", "healthStatus");
        patient.setHealthStatus(HealthStatus.UNHEALTHY);
        return this.patientDao.addPatient(patient).convertToDTO();
    }

    public PatientDto dischargeOrCharge(Long id, String healthStatus)
        throws PatientWithIdNotFoundException, PatientWithIdCanNotBeDischarge {
        if (healthStatus.equals(HealthStatus.HEALTHY.getHealthStatus())) {
            return this.dischargePatient(id);
        }
        return this.chargePatient(id);
    }

    private PatientDto chargePatient(Long id) throws PatientWithIdNotFoundException{
        Patient patient = this.getPatientEntity(id);
        if (patient == null) {
            throw new PatientWithIdNotFoundException(id);
        }
        patient.setHealthStatus(HealthStatus.UNHEALTHY);
        return this.patientDao.updatePatient(patient).convertToDTO();
    }


    public PatientDto dischargePatient(Long id) throws PatientWithIdCanNotBeDischarge, PatientWithIdNotFoundException {
        Patient patient = this.getPatientEntity(id);
        if (patient == null) {
            throw new PatientWithIdNotFoundException(id);
        }
        List<Event> performedOrCancelledEvents = new LinkedList<>();
        List<Event> allEventsByPatient = this.eventDao.getAllEventsByPatient(id);
        for (Event event : allEventsByPatient) {
            if (event.getEventStatus().equals(EventStatus.PERFORMED) &&
            event.getEventStatus().equals(EventStatus.CANCELED)) {
                performedOrCancelledEvents.add(event);
            }
        }
        if (performedOrCancelledEvents.size() != allEventsByPatient.size()) {
            throw new PatientWithIdCanNotBeDischarge(id);
        }
        patient.setHealthStatus(HealthStatus.HEALTHY);
        return this.patientDao.updatePatient(patient).convertToDTO();
    }
}
