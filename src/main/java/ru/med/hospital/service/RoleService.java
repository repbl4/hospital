package ru.med.hospital.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.med.hospital.dao.RoleDao;
import ru.med.hospital.model.Role;

@Service
@RequiredArgsConstructor
public class RoleService {
	private final RoleDao roleDao;

	public Role getRoleByName(String roleName) {
		return this.roleDao.getRoleByName(roleName);
	}
}
