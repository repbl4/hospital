package ru.med.hospital.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import ru.med.hospital.model.Role;
import ru.med.hospital.model.enums.Roles;
import ru.med.hospital.security.JwtUserServiceDetail;
import ru.med.hospital.security.jwt.JwtConfigurer;
import ru.med.hospital.security.jwt.JwtTokenProvider;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true, proxyTargetClass = true)
@PropertySource("classpath:application.properties")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	private final JwtTokenProvider jwtTokenProvider;
	private final JwtUserServiceDetail jwtUserServiceDetail;
	private final PasswordEncoder passwordEncoder;

	@Value("${webappOrigin}")
	private String webappOrigin;

	private final String admin = Roles.ADMIN.getRoleName();
	private final String doctor = Roles.DOCTOR.getRoleName();
	private final String nurse = Roles.NURSE.getRoleName();

	@Override
     public void configure(WebSecurity web) throws Exception {
         web.ignoring()
	         .antMatchers("/v2/api-docs", "/swagger-resources/**", "/configuration/security", "/webjars/**");
     }

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(jwtUserServiceDetail).passwordEncoder(this.passwordEncoder);
	}


	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.httpBasic().disable()
				.csrf().disable()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
				.authorizeRequests()
				.antMatchers(HttpMethod.POST,"/auth/login").permitAll()
				.antMatchers(HttpMethod.POST, "/auth/registration").hasRole(admin)
				.antMatchers(HttpMethod.GET, "/users/**").hasRole(admin)

				.antMatchers(HttpMethod.GET, "/patients/{id}").hasRole(doctor)
				.antMatchers(HttpMethod.GET, "/patients/healthy/**").hasAnyRole(doctor, nurse)
				.antMatchers(HttpMethod.GET, "/patients").hasAnyRole(doctor, nurse)
				.antMatchers(HttpMethod.PUT, "/patients/{id}").hasRole(doctor)
				.antMatchers(HttpMethod.POST, "/patients").hasRole(doctor)

				.antMatchers(HttpMethod.GET, "/patients/appointments/{id}").hasRole(doctor)
				.antMatchers(HttpMethod.GET, "/patients/{patientId}/appointments").hasRole(doctor)
				.antMatchers(HttpMethod.POST, "/patients/{patientId}/appointments").hasRole(doctor)
				.antMatchers(HttpMethod.PUT, "/patients/appointments/{id}").hasRole(doctor)

				.antMatchers(HttpMethod.GET, "/events").hasRole(nurse)
				.antMatchers(HttpMethod.GET, "/events/today").hasRole(nurse)
				.antMatchers(HttpMethod.GET, "events/next-three-hours").hasRole(nurse)
				.antMatchers(HttpMethod.GET, "/events/{id}").hasRole(nurse)
				.antMatchers(HttpMethod.GET, "/events/patient/{id}").hasRole(nurse)
				.antMatchers(HttpMethod.PUT, "/events/{id}").hasRole(nurse)

				.antMatchers("/webjars/**", "/swagger-ui.html/**").permitAll()
				.anyRequest().authenticated()
			.and()
				.cors().configurationSource(corsConfigurationSource())
			.and()
				.apply(new JwtConfigurer(jwtTokenProvider));
	}


	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.applyPermitDefaultValues();
		config.setAllowedOrigins(Arrays.asList(this.webappOrigin, "http://192.168.1.101:4200", "http://192.168.1.102:4200",
			"http://localhost:4200"));
		config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS", "HEAD"));
		source.registerCorsConfiguration("/**", config);
		return source;
	}


}
