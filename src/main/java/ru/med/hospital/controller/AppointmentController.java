package ru.med.hospital.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.med.hospital.dto.AppointmentDto;
import ru.med.hospital.dto.UpdatedAppointDto;
import ru.med.hospital.exception.AppointmentNotFound;
import ru.med.hospital.exception.PatientWithIdNotFoundException;
import ru.med.hospital.exception.TimePatternNotFoundException;
import ru.med.hospital.model.Appointment;
import ru.med.hospital.service.AppointmentService;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/patients")
public class AppointmentController {
    private final AppointmentService appointmentService;


    @GetMapping("/appointments/{id}")
    public ResponseEntity<AppointmentDto> getAppointment(@PathVariable("id") Long id) {
        return new ResponseEntity<>(this.appointmentService.getAppointmentDto(id), HttpStatus.OK);
    }

    @GetMapping("/{patientId}/appointments")
    public ResponseEntity<List<AppointmentDto>> getAppointmentsByPatientsId
        (@PathVariable("patientId") Long patientId) {
        return new ResponseEntity<>(this.appointmentService.getAppointmentsByPatientId(patientId), HttpStatus.OK);
    }


    @PostMapping("/{patientId}/appointments")
    public ResponseEntity<AppointmentDto> createAppointment(
        @RequestBody AppointmentDto dto) throws PatientWithIdNotFoundException {
        return new ResponseEntity<>(this.appointmentService.createAppointment(dto), HttpStatus.CREATED);
    }

    @PutMapping("/appointments/{id}")
    public ResponseEntity<AppointmentDto> updateAppointment(
        @PathVariable("id") Long id, @RequestBody UpdatedAppointDto dto)
    throws TimePatternNotFoundException, AppointmentNotFound {
        return new ResponseEntity<>(this.appointmentService.updateAppointment(id, dto), HttpStatus.OK);
    }
}
