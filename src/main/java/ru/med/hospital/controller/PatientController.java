package ru.med.hospital.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.med.hospital.dto.PatientDto;
import ru.med.hospital.exception.PatientWithIdCanNotBeDischarge;
import ru.med.hospital.exception.PatientWithIdNotFoundException;
import ru.med.hospital.service.PatientService;

import java.util.List;

@RestController
@RequestMapping("/patients")
@RequiredArgsConstructor
public class PatientController {
    private final PatientService patientService;

    @GetMapping("/{id}")
    public ResponseEntity<PatientDto> getPatient(
            @PathVariable("id") Long id) throws PatientWithIdNotFoundException {
        return new ResponseEntity<>(this.patientService
            .getPatientDtoById(id), HttpStatus.OK);
    }

    @GetMapping("/healthy")
    public ResponseEntity<List<PatientDto>> getAllPatientsByStatus(
        @RequestParam(value = "health-status") String healthStatus) {
        return new ResponseEntity<>(this.patientService.getAllPatientsByStatus(healthStatus), HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<List<PatientDto>> getPatientList() {
        return new ResponseEntity<>(this.patientService.getPatientsList(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<PatientDto> dischargeOrChargePatient(
        @PathVariable("id") Long id, @RequestBody String healthStatus)
        throws PatientWithIdCanNotBeDischarge, PatientWithIdNotFoundException {
        return new ResponseEntity<>(this.patientService.dischargeOrCharge(id, healthStatus), HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<PatientDto> addPatient(@RequestBody PatientDto dto) {
        return new ResponseEntity<>(this.patientService.addPatient(dto), HttpStatus.CREATED);
    }
}
