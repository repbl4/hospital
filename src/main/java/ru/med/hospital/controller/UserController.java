package ru.med.hospital.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.med.hospital.dto.AuthResponse;
import ru.med.hospital.dto.RegisterUserDto;
import ru.med.hospital.dto.UserDto;
import ru.med.hospital.dto.UserLoginDto;
import ru.med.hospital.exception.UserAlreadyExistException;
import ru.med.hospital.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class UserController {
	private final UserService userService;

	@GetMapping("/users")
	public ResponseEntity<List<UserDto>> getAllUsers() {
		return new ResponseEntity<>(this.userService.getAllUsers(), HttpStatus.OK);
	}

	@PostMapping("/auth/registration")
	public ResponseEntity<UserDto> createUser(
		@RequestBody RegisterUserDto registerUserDto) throws UserAlreadyExistException {
		return new ResponseEntity<>(this.userService.registerUser(registerUserDto), HttpStatus.CREATED);
	}

	@PostMapping("/auth/login")
	public ResponseEntity<AuthResponse> login(@RequestBody UserLoginDto userLoginDto) {
		return new ResponseEntity<>(this.userService.login(userLoginDto), HttpStatus.OK);
	}
}
