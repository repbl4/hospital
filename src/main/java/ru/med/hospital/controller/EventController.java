package ru.med.hospital.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.med.hospital.dto.EventDto;
import ru.med.hospital.exception.EventStatusNotFoundException;
import ru.med.hospital.service.EventService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/events")
public class EventController {
    private final EventService eventService;

    @GetMapping
    public ResponseEntity<List<EventDto>> getAllEvents() {
        return new ResponseEntity<>(this.eventService.getAllEvents(), HttpStatus.OK);
    }
    @GetMapping("/today")
    public ResponseEntity<List<EventDto>> getAllEventsForToday() {
        return new ResponseEntity<>(this.eventService.getEventForToday(), HttpStatus.OK);
    }

    @GetMapping("/next-three-hours")
    public ResponseEntity<List<EventDto>> getAllEventsDuringNextThreeHours() {
        return new ResponseEntity<>(this.eventService.getEventsDuringNextThreeHours(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EventDto> getEvent(@PathVariable("id") Long id) {
        return new ResponseEntity<>(this.eventService.getEventDto(id), HttpStatus.OK);
    }

    @GetMapping("/patient/{id}")
    public ResponseEntity<List<EventDto>> getAllEventsByPatient(@PathVariable("id") Long id) {
        return new ResponseEntity<>(this.eventService.getAllEventsByPatient(id), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<EventDto> updateEvent(
        @PathVariable("id") Long id, @RequestBody EventDto dto) throws EventStatusNotFoundException {
        return new ResponseEntity<>(this.eventService.updateEvent(id, dto), HttpStatus.OK);
    }
}
