package ru.med.hospital.exception;

public class PatientWithIdNotFoundException extends Throwable {
	private final static String ERROR_TEXT = "Patient with id: %s not found";
	public PatientWithIdNotFoundException(Long id) {
		super(String.format(ERROR_TEXT, id));
	}
}
