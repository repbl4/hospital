package ru.med.hospital.exception;

public class TimePatternNotFoundException extends Throwable {
	private static final String ERROR_TEXT = "Time pattern not found";

	public TimePatternNotFoundException() {
		super(ERROR_TEXT);
	}
}
