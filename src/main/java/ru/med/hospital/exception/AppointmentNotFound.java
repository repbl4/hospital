package ru.med.hospital.exception;

public class AppointmentNotFound extends Throwable {
	private static final String ERROR_TEXT = "Appointment has not found or has had cancelled status";

	public AppointmentNotFound() {
		super(ERROR_TEXT);
	}
}
