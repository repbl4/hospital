package ru.med.hospital.exception;

public class PatientWithIdCanNotBeDischarge extends Throwable {
	private final static String ERROR_TEXT = "Patient with id: %s can not be discharged";

	public PatientWithIdCanNotBeDischarge(Long id) {
		super(String.format(ERROR_TEXT, id));
	}
}
