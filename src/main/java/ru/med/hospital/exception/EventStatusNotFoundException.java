package ru.med.hospital.exception;

public class EventStatusNotFoundException extends Throwable {
	private final static String ERROR_TEXT = "Event status named: %s not found";
	public EventStatusNotFoundException(String eventName) {
		super(String.format(ERROR_TEXT, eventName));
	}
}
