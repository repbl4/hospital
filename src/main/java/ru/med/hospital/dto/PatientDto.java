package ru.med.hospital.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatientDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String diagnostic;
    private String insuranceNumber;
    private String attachedDoctor;
    private String healthStatus;
}
