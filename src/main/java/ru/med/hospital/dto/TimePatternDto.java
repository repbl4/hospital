package ru.med.hospital.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimePatternDto {
    private List<String> timeOfDay;
    private List<String> nameOfDays;
}
