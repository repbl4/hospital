package ru.med.hospital.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO для создания назначений.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentDto {
    private Long id;
    private Long patientId;
    private String type;
    private String name;
    private TimePatternDto timePatternDto;
    private String period;
    private String dosage;
    private String status;

}
