package ru.med.hospital.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventDto {
    private Long id;
    private Date eventTime;
    private String status;
    private String appointmentName;
    private String appointmentType;
    private String patientName;
    private String patientSurname;
    private String comment;
}
