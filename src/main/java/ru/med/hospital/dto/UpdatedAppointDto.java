package ru.med.hospital.dto;

import lombok.Data;

/**
 * DTO для обновления назначений.
 */

@Data
public class UpdatedAppointDto {
    private String name;
    private String period;
    private String dosage;
    private TimePatternDto timePatternDto;
    private String status;
}
