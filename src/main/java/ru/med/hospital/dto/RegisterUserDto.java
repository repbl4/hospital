package ru.med.hospital.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterUserDto {
	private String username;
	private String role;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
}
