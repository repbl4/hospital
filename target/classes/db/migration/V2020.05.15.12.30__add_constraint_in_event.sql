alter table event
    add column patient_id bigint not null,
    add constraint FK_event_patient
    foreign key (patient_id)
    references patient (id);